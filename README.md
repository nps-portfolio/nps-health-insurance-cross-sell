# Health Insurance Cross Sell

**Disclaimer: The following Context is completely fictional**

Insurance All is a company that provides health insurance to its customers and the product team is analyzing the possibility of offering policyholders a new product: Auto insurance.

As with health insurance, customers of this new auto insurance plan need to pay an amount annually to Insurance All to obtain an amount insured by the company, intended for the costs of an eventual accident or damage to the vehicle.

# 1. Business Problem.

Insurance All surveyed nearly 380,000 customers about their interest in joining a new auto insurance product last year. All customers showed interest or not in purchasing auto insurance and these responses were saved in a database along with other customer attributes.

The product team selected 127,000 new customers who did not respond to the survey to participate in a campaign, in which they will be offered the new auto insurance product. The offer will be made by the sales team through phone calls.

However, the sales team has the capacity to make 20,000 calls within the campaign period.

# 2. Business Assumptions.

I only considered the information provided through the data

I considered that ranking should be done on new data based on history

I considered that any result at least twice as good as random use would be enough for this first study.
# 3. Solution Strategy

My strategy to solve this challenge was:

**Step 01. Data Description:** My goal is to use statistics metrics to identify data outside the scope of business.

**Step 02. Feature Engineering:** Derive new attributes based on the original variables to better describe the phenomenon that will be modeled.

**Step 03. Data Filtering:** Filter rows and select columns that do not contain information for modeling or that do not match the scope of the business.

**Step 04. Exploratory Data Analysis:** Explore the data to find insights and better understand the impact of variables on model learning.

**Step 05. Data Preparation:** Prepare the data so that the Machine Learning models can learn the specific behavior.

**Step 06. Feature Selection:**  Selection of the most significant attributes for training the model.

**Step 07. Machine Learning Modelling:** Machine Learning model training

**Step 08. Hyperparameter Fine Tunning:** Choose the best values for each of the parameters of the model selected from the previous step.

**Step 09. Convert Model Performance to Business Values:** Convert the performance of the Machine Learning model into a business result.

**Step 10. Deploy Modelo to Production:** Publish the model in a cloud environment so that other people or services can use the results to improve the business decision.

# 4. Top 3 Data Insights

**Hypothesis 01:** Owners of damaged vehicles are more interested in purchasing insurance - **True**

**Hypothesis 02:** New vehicles have more insurance than older vehicles - **True**

**Hypothesis 03:**  Insurance for women is more expensive on average than for men - **False**

# 5. Machine Learning Model Applied
Four models were evaluated: KNN, Extra Tree, Logistic Regression and LightGBM.

After comparing precision and recall metrics, the model chosen was the **LightGBM Classifier**.
# 6. Machine Learning Modelo Performance

The model used allows that, using 50% of the database, it is possible to identify all customers interested in insurance.

From this percentage of data, using the model recommendations proved to be at least twice as good as performing random calls.

![image](src/visualization/cumulative-gain.png)

# 7. Business Results

- Com 20.000 ligações a equipe de vendas conseguirá atingir 30.32% dos clientes interessados, ou seja, 6064.0 clientes.

- Com 40.000 ligações a equipe de vendas conseguirá atingir 12.37% dos clientes interessados, ou seja, 2474.0 clientes.

# 8. Conclusions

Using ranking learning techniques allows more accurate recommendations to be made based on previous patterns in the data.

Using classification algorithms, it was possible to provide the sales team with a list with priority names, saving time and optimizing available financial resources.
# 9. Lessons Learned

Use ranking algorithms to create recommendations.

Implement and evaluate ranking metrics

Transform model performance into business performance

Implement an API to make the model available for use

# 10. Next Steps to Improve

Better understand the business with the intention of identifying and analyzing new hypotheses

Check the possibility, based on new knowledge, to develop new features

Carry out a new EDA based on the new features

Test new encoders for variables

Use boruta to select relevant features

Use other classification models and evaluate the results

Implement new ranking metrics

Implement the api with docker and CI/CD in gitlab